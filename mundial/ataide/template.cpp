/*%
<Nome do algoritmo>
Autor: <autor ou fonte da implementacao>
Complexidade: <complexidade>
Tempo de implementacao: <Tempo aprox de implementacao>
Testes: <Campo de teste (em quais problemas passou)>
Dependencias: <Dependencias de outras partes do notebook>
Descricao: <Pra que serve o algoritmo>
%*/

int f() {
  return 12345;
}


/**** Exemplo simples de uso ****/
int main(void){
        printf("12345 = %d\n",f());
        return 0;
}
