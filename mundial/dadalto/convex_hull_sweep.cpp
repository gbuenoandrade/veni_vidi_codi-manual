/*%
Convex Hull e Point Query
Autor: Arthur Dadalto
Complexidade: O(n log n)
Tempo de implementacao: 
Testes: 
Dependencias: 
Descricao: Encontra o convex hull com line sweep 
e responde várias queries se um ponto está dentro do convex hull
%*/
struct point{
  long long x, y;
  point(long long _x = 0, long long _y = 0) : x(_x), y(_y) {}
  long long cross(point p1, point p2) {
    return (p1.x - x)*(p2.y - y) - (p2.x - x)*(p1.y - y);
  }
  bool operator < (point b) const {
    if(x != b.x)
      return x < b.x;
    return y > b.y;
  }
} p[112345];

point ch[112345];

bool comp(point a, point b){ return a.x < b.x; }

int main(void)
{
  int n, q;
  scanf("%d %d", &n, &q);
  for(int i = 0; i < n; i++)
    scanf("%lld %lld", &p[i].x, &p[i].y);
  sort(p, p+n);
  vector<point> upper, lower;
  upper.push_back(p[0]), lower.push_back(p[0]);
  upper.push_back(p[1]), lower.push_back(p[1]);
  for(int i = 2; i < n; i++)
  {
    while(upper.size() >= 2
      && upper[upper.size() - 2].cross(upper.back(), p[i]) >= 0)
      upper.pop_back();
    upper.push_back(p[i]);
    while(lower.size() >= 2
      && lower[lower.size() - 2].cross(lower.back(), p[i]) <= 0)
      lower.pop_back();
    lower.push_back(p[i]);
  }
  int ans = 0;
  while(q--)
  {
    point o;
    scanf("%lld %lld", &o.x, &o.y);
    if(o.x < upper[0].x || o.x > upper.back().x)
      continue;
    int i = lower_bound(upper.begin(), upper.end(), o, comp) 
    - upper.begin();
    int j = lower_bound(lower.begin(), lower.end(), o, comp) 
    - lower.begin();

    if((i != 0 && upper[i-1].cross(upper[i], o) > 0) 
      || (i + 1 < upper.size() && upper[i].cross(upper[i+1], o) > 0) 
      || (i + 2 < upper.size() && upper[i+1].cross(upper[i+2], o) > 0))
      continue;
    if((j != 0 && lower[j-1].cross(lower[j], o) < 0) 
      || (j + 1 < lower.size() && lower[j].cross(lower[j+1], o) < 0) 
      || (j + 2 < lower.size() && lower[j+1].cross(lower[j+2], o) < 0))
      continue;
    ans++;
  }
  printf("%d\n", ans);
}

