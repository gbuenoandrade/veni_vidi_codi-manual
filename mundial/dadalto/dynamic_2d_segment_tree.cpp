/*%
Segment Tree 2D
Autor: Arthur Dadalto
Complexidade: O(n log^2(MAX_COORDENADA))
Tempo de implementacao: 
Testes: 
Dependencias: 
Descricao: Implementa uma seg 2D com update em uma célula e 
query em um retângulo
%*/
int n, m;

long long join(long long x, long long y) {
  return x+y;
}

struct nodes {
  long long val;
  nodes *left, *right;
  nodes() : left(NULL), right(NULL), val (0) {}

  void update(int l, int r, int a, long long x)
  {
    if(l == r)
      val = x;
    else
    {
      int mid = (l+r)/2;
      if(a <= mid)
      {
        if(left == NULL) left = new nodes;
        left->update(l, mid, a, x);
      }
      else
      {
        if(right == NULL) right = new nodes;
        right->update(mid+1, r, a, x);
      }
      val = join(left ? left->val : 0, right ? right->val : 0);
    }
  }

  void updateb(int l, int r, int a, long long x, nodes *o, nodes *p)
  {
    if(l == r)
      val = join(o ? o->val : 0, p ? p->val : 0);
    else
    {
      int mid = (l+r)/2;
      if(a <= mid)
      {
        if(left == NULL) left = new nodes;
        left->updateb(l, mid, a, x, o ? o->left : NULL, p ? p->left : NULL);
      }
      else
      {
        if(right == NULL) right = new nodes;
        right->updateb(mid+1, r, a, x, o ? o->right : NULL, p ? p->right : NULL);
      }
      val = join(o ? o->val : 0, p ? p->val : 0);
    }
  }

  long long get(int l, int r, int a, int b)
  {
    if(l == a && r == b)
      return val;
    else
    {
      int mid = (l+r)/2;
      if(b <= mid)
        return left ? left->get(l, mid, a, b) : 0;
      else if(a > mid)
        return right ? right->get(mid+1, r, a, b) : 0;
      else
        return join(left ? left->get(l, mid, a, mid) : 0, right ? right->get(mid+1, r, mid+1, b) : 0);
    }
  }
};

struct nodef {
  nodes *val;
  nodef *left, *right;
  nodef() : left(NULL), right(NULL) { val = new nodes; }

  void update(int l, int r, int a, int b, long long x)
  {
    if(l == r)
      val->update(0, m, b, x);
    else
    {
      int mid = (l+r)/2;
      if(a <= mid)
      {
        if(left == NULL) left = new nodef;
        left->update(l, mid, a, b, x);
      }
      else
      {
        if(right == NULL) right = new nodef;
        right->update(mid+1, r, a, b, x);
      }
      val->updateb(0, m, b, x, left ? left->val : NULL, right ? right->val : NULL);
    }
  }

  long long get(int l, int r, int a, int b, int c, int d)
  {
    if(l == a && r == b)
      return val->get(0, m, c, d);
    else
    {
      int mid = (l+r)/2;
      if(b <= mid)
        return left ? left->get(l, mid, a, b, c, d) : 0;
      else if(a > mid)
        return right ? right->get(mid+1, r, a, b, c, d) : 0;
      else
        return join(left ? left->get(l, mid, a, mid, c, d) : 0, right ? right->get(mid+1, r, mid+1, b, c, d) : 0);
    }
  }
};

int main(void)
{
  long long a;
  nodef *root = new nodef;
  int q, tp, x, y, z, w;
  scanf("%d %d %d", &n, &m, &q);
  while(q--)
  {
    scanf("%d", &tp);
    if(tp == 1)
    {
      scanf("%d %d %lld", &x, &y, &a);
      root->update(0, n, x, y, a);
    }
    else
    {
      scanf("%d %d %d %d", &x, &y, &z, &w);
      printf("%lld\n", root->get(0, n, x, z, y, w));
    }
  }
}
