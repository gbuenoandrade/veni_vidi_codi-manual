/*%
Merge Sort Tree
Autor: Arthur Dadalto
Complexidade: O(n log n)
Tempo de implementacao: 
Testes: 
Dependencias: 
Descricao: Constroi a mergesort tree para responder 
queries sobre um intervalo se ele estivesse ordenado.
%*/
#define MAXN 112345
#define left(i) ((i)<<1)
#define right(i) (((i) << 1) + 1)
typedef pair<int,int> pii;
 
vector<int> val[4 * MAXN];
pii a[MAXN];
int ans[MAXN];
 
void build(int id, int l, int r)
{
  if(l == r)
    val[id].push_back(a[l].second);
  else
  {
    int mid = (l+r)/2;
    build(left(id), l, mid), build(right(id), mid+1, r);
    val[id] = vector<int>(r-l+1);
    merge(val[left(id)].begin(), val[left(id)].end(),
      val[right(id)].begin(), val[right(id)].end(), val[id].begin());
  }
}
 
int count_interval(int id, int a, int b) {
  return (upper_bound(val[id].begin(), val[id].end(), b)
    - lower_bound(val[id].begin(), val[id].end(), a));
}
 
int get(int id, int l, int r, int a, int b, int x)
{
  if(l == r)
    return ans[val[id][0]];
  int mid = (l+r)/2;
  if(count_interval(left(id), a, b) >= x)
    return get(left(id), l, mid, a, b, x);
  else
    return get(right(id), mid+1, r, a, b, x 
      - count_interval(left(id), a, b));
}
 
 
int main(void)
{
  int n, q, x, y, z;
  scanf("%d %d", &n, &q);
  for(int i = 0; i < n; ans[i] = a[i].first, a[i].second = i, i++)
    scanf("%d", &a[i].first);
  sort(a, a+n);
  build(1, 0, n-1);
  for(int i = 0; i < q; i++)
  {
    scanf("%d %d %d", &x, &y, &z);
    printf("%d\n", get(1, 0, n-1, x-1, y-1, z));
  }
}
