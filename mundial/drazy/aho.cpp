#define MAXS 1000
#define MAXT 100000
#define MAX 100000
#define cc 52

int T[MAX], term[MAX], sig[MAX][cc], cnt;

void add(char s[MAXS], int id) {
	int x=0, n=strlen(s);
	FOR0(i, n) {
		int c = s[i]-'A';
		if (sig[x][c]==0) {
			term[cnt]=0, sig[x][c] = cnt++;
		}
		x = sig[x][c];
	}
	term[x] = 1;
}

void aho() {
	queue<int> Q;
	FOR0(i,cc) {
		int v = sig[0][i];
		if (v) Q.push(v), T[v]=0;
	}
	while(!Q.empty()) {
		int u=Q.front(); Q.pop();
		FOR0(i,cc) {
			int x=sig[u][i];
			if (!x) continue;
			int v=T[u];
			while (!sig[v][i] && v) v=T[v];
			int y=sig[v][i];
			Q.push(x), T[x]=y, term[x]|=term[y];
		}
	}
}