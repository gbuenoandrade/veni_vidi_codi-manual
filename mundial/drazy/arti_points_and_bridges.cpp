/*%
Articulation Points and Bridges
Complexidade: O(n+m)
%*/
const int MAXN = 100010;
vi adj[MAXN];
int d[MAXN], low[MAXN], visi[MAXN], t;
bool art[MAXN];
set<pii> bs;

//call with FOR0(i,n) if (!visi[i]) dfs(i);
void dfs(int u, int p=-1) {
  visi[u] = true;
  d[u] = low[u] = t++;
  bool found = false;
  int ct=0;
  for(auto v : adj[u]) {
    if(!visi[v]) {
      ++ct;
      dfs(v,u);
      low[u] = min(low[u],low[v]);
      if(low[v]>=d[u]) found = true;
      if(low[v]>d[u]) bs.insert(mp(min(u,v),max(u,v)));
    }
    else if(v!=p) {
      low[u] = min(low[u], d[v]);
    }
  }
  art[u] = (u ? found : ct>1);
}