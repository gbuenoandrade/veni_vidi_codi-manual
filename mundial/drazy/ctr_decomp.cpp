/*%
Centroid Decomposition
Complexidade: O(nlogn)
%*/
vi adj[MAXN];
int rnd[MAXN], qtd[MAXN];

int dfs(int src, int par) {
  int ans = 1;
  for (int x : adj[src]) if (x!=par && !rnd[x])
    ans += dfs(x, src);
  return qtd[src] = ans; 
}

void solve(int src, int crnd) {
  int ctr = src;
  int par = -1;
  int tgt = dfs(src, -1)/2; 
  while (true) {
    bool found = false;
    for (int x : adj[ctr]) if (x!=par && !rnd[x] && qtd[x]>tgt) {
      par = ctr;
      ctr = x;
      found = true;
      break;
    }
    if (!found) break;
  }
  rnd[ctr] = crnd;
  for (int x : adj[ctr]) if (!rnd[x]) solve(x,crnd+1);
}