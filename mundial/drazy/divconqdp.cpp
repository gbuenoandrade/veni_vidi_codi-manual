/*%
Divide and Conquer DP
Complexidade: O(nlogn)
%*/
const ll INFINITE = 1e17;

ll dp[8010][810], v[8010];
int n;

ll cost(int x, int y) {
  if(x>y) return 0;
  ll sum = v[y] - (x ? v[x-1] : 0LL);
  return sum*(y-x+1);
}

void fill(int g, int lmin, int lmax, int pmin, int pmax) {
  if(lmin>lmax) return;
  int lmid = (lmin+lmax)/2;
  dp[lmid][g] = INFINITE;
  ll minp;
  FOR(i,pmin, pmax+1) {
    ll ncost = dp[i][g-1] + cost(i+1,lmid);
    if(ncost < dp[lmid][g]) {
      dp[lmid][g] = ncost;
      minp = i;
    }
  }
  fill(g,lmin,lmid-1,pmin,minp);
  fill(g,lmid+1,lmax,minp,pmax);
}