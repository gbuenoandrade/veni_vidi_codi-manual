/*%
Dual String Hash (with iseq and isless)
Complexidade: O(n + logn)
%*/
const int MAXN = 5000;
const int QTD = 2;
int B[] = {31,97};
int MODH[] = {1000000007,1000000009};
int pwr[MAXN][QTD];
int h[MAXN][QTD];
string s;
int n;

void precalc() {
  FOR0(j,QTD) {
    h[0][j] = s[0]%MODH[j];
    pwr[0][j] = 1%MODH[j];
    FOR(i,1,n) {
      h[i][j] = (h[i-1][j]*1LL*B[j] + s[i])%MODH[j];    
      pwr[i][j] = pwr[i-1][j]*1LL*B[j]%MODH[j];
    }
  }
}

void calc(int x, int y, int ans[QTD]) {
  if (x>y) return;
  FOR0(j,QTD) {
    ans[j] =(h[y][j]-(x?h[x-1][j]:0)*1LL*pwr[y-x+1][j])%MODH[j];
    if (ans[j] < 0) ans[j] += MODH[j];
  }
}

bool is_eq(int x1, int y1, int x2, int y2) {
  int aux1[QTD], aux2[QTD];
  calc(x1,y1,aux1);
  calc(x2,y2,aux2);
  FOR0(i,QTD) if (aux1[i] != aux2[i]) return false;
  return true;
}

bool is_less(int x1, int y1, int x2, int y2) {
  int sz1 = y1-x1+1, sz2 = y2-x2+1;
  int beg = 0, end = min(sz1,sz2);
  while (beg < end) {
    int mid = (beg+end)/2;
    if (!is_eq(x1,x1+mid, x2, x2+mid)) end = mid;
    else beg = mid + 1;
  }
  if (end == min(sz1,sz2)) return (sz1 < sz2);
  return s[x1+end] < s[x2+end];
}