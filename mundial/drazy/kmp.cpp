/*%
KMP
Complexidade: O(n)
%*/
int pref[MAXN];
void calc_pref(string s) {
  int n = sz(s);
  for (int i=1, j=0; i<n; ++i) {
    while (j && s[j] != s[i]) j = pref[j-1];
    if (s[i]==s[j]) ++j;
    pref[i] = j;
  }
}