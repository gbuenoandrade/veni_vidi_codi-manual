/*%
LCA
Complexidade: O(nlogn)
%*/
const int MAXN = 100010;
const int LOGMAXN = 18;
vi adj[MAXN];
int par[MAXN], l[MAXN];
int p[MAXN][LOGMAXN];
int n;

void dfs(int src) {
  if(src) l[src] = l[par[src]] + 1;
  TRAV(it,adj[src]) if(par[*it]==-1) {
    par[*it] = src;
    dfs(*it);
  }
}

void preproc() {
  SET(par);
  l[0] = 0;
  par[0]=0;
  dfs(0);
  SET(p);
  for(int j=0; (1<<j)<n; ++j) FOR0(i,n) {
    if(!j) p[i][j] = par[i];
    else if(p[i][j-1]!=-1) {
      p[i][j] = p[p[i][j-1]][j-1];
    }
  }
}

int lca(int x, int y) {
  if(x==y) return x;
  if(l[x]>l[y]) swap(x,y);
  int log = 0;
  while((1<<(log+1)) <= l[y]) ++log;
  int dl = l[y] - l[x];
  if(dl) for(int i=log; i>=0; --i) if(dl&(1<<i)) y = p[y][i];
  if(x==y) return x;
  for(int i=log; i>=0; --i) if(p[x][i]!=p[y][i]) {
    x = p[x][i];
    y = p[y][i];
  }
  return p[x][0];
}
