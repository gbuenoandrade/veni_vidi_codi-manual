/*%
Regular Convex Hull Trick
Complexidade: O(n + logn)
%*/
typedef long double ld;
struct node {
  ld left, p, q;
};
vector<node> lines;

ld get_inter(ld p, ld q, ld r, ld s) { return (q-s)/(r-p); }

// lines must be inserted in strictly decreasing order of p
void insert(ld p, ld q) {
  while (sz(lines) >= 2) {
    ld r = lines[sz(lines)-2].p;
    ld s = lines[sz(lines)-2].q;
    if (lines.back().left > get_inter(p,q,r,s)) lines.pop_back();
    else break;
  }
  node nd;
  nd.p = p; nd.q = q;
  nd.left = (lines.empty() ?
            -INFINITY :
            get_inter(lines.back().p, lines.back().q, p, q));
  lines.pb(nd);
}

ld get_min(ld x) {
  node aux;
  aux.left = x;
  auto nd = *prev(upper_bound(all(lines),aux,[](node a, node b){
    return a.left < b.left;
  }));
  return nd.p*x + nd.q;
}
