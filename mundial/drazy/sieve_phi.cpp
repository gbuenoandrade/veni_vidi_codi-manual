/*%
Sieve / Phi / Integer Factorization
Complexidade: O(nloglogn)
%*/
const int MAXN = 10000010;
int pr[MAXN];
int divisor[MAXN];
int phi[MAXN];

void sieve(int n) {
  FOR0(i,n+1) pr[i] = true;
  pr[0] = pr[1] = false;
  for(int i=2; i*i<=n; ++i) {
    if(!pr[i]) continue;
    int k = i*i;
    while(k<=n) {
      divisor[k] = i;
      pr[k] = false;
      k += i;
    }
  }
}

void calc_phi(int n) {
  FOR(i,1,n+1) phi[i] = i;
  FOR(i,2,n+1) if(pr[i]) {
    for(int j=i; j<=n; j+=i) {
      phi[j] -= phi[j]/i;
    }
  }
}

inline int get_div(int n) {
  if(pr[n]) return n;
  return divisor[n];
}

int factorize(int v[], int n) {
  if(n<=1) return 0;  
  int sz=0;
  while(n>1) {
    int p = get_div(n);
    v[sz++] = p;
    n/=p;
  }
  return sz;
}