/*%
Sqrt decomposition
Autor: <autor ou fonte da implementacao>
Complexidade: O(nsqrtn)
Tempo de implementacao: <Tempo aprox de implementacao>
Testes: <Campo de teste (em quais problemas passou)>
Dependencias: <Dependencias de outras partes do notebook>
Descricao: <Pra que serve o algoritmo>
%*/
const int MAXN = 1010;
const int SQRTN = 35;
int v[MAXN];
int blocks[SQRTN + 10];
int n;

void build_sqrt() {
  SET(blocks);
  FOR0(i,n) {
    int b = i/SQRTN;
    if(blocks[b]==-1 || v[i]<v[blocks[b]]) {
      blocks[b] = i;
    }
  }
}

void update_sqrt(int x, int vx) {
  v[x] = vx;
  int b = x/SQRTN;
  if(vx < v[blocks[b]]) blocks[b] = x;
}

int query_sqrt(int x, int y) {
  int ans;
  int minv = INF;
  int i = x;
  while(i<=y && i%SQRTN) {
    if(v[i]<minv) {
      minv = v[i];
      ans = i;
    }
    ++i;
  }
  while(i+SQRTN-1 <= y) {
    int b = i/SQRTN;
    if(v[blocks[b]] < minv) {
      minv = v[blocks[b]];
      ans = blocks[b];
    }
    i += SQRTN;
  }
  while(i<=y) {
    if(v[i]<minv) {
      minv = v[i];
      ans = i;
    }
    ++i;
  }
  return ans;
}