/*%
Suffix Array
Complexidade: O(nlogn)
%*/
const int MAXN = 200010;
const int LOGMAXN = 20; // > floor(log(MAXN))
int p[LOGMAXN][MAXN];
int sa[MAXN];
int step;
pair<pii,int> vaux[MAXN];
string s;

void calcsa() {
  int n = s.size();
  FOR0(i,n) p[0][i] = s[i];
  if(n==1) p[0][0] = 0; 
  int pot;
  for(step=1,pot=2; pot<2*n; ++step, pot*=2) {
    FOR0(i,n) {
      vaux[i].first.first = p[step-1][i];
      vaux[i].first.second = i+pot/2<n ? p[step-1][i+pot/2]:-1; 
      vaux[i].second = i;
    }
    sort(vaux,vaux+n);
    int id = 0;
    FOR0(i,n) {
      if(i && vaux[i].first != vaux[i-1].first) ++id;
      p[step][vaux[i].second] = id;
    }
  }
  --step;
  FOR0(i,n) {
    sa[p[step][i]] = i;
  } 
}
//sa[i] = index of the i-th suffix

int lcp(int x, int y) { //x,y are idx of suffix
  int n = s.size();
  if(x==y) return n-x;
  int ans = 0;
  for(int i=step; i>=0; --i) {
    if(p[i][x]==p[i][y]) {
      ans |= (1<<i);
      x += (1<<i);
      y += (1<<i);
      if(x>=n || y>=n) break;
    }
  }
  return ans;
}