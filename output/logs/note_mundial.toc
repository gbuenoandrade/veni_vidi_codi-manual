\contentsline {section}{\numberline {1}Grafos}{1}
\contentsline {subsection}{\numberline {1.1}\IeC {\'A}rvore de Steiner}{1}
\contentsline {subsection}{\numberline {1.2}\IeC {\'A}rvore Geradora M\IeC {\'\i }nima (Prim)}{1}
\contentsline {subsection}{\numberline {1.3}Bellman Ford}{1}
\contentsline {subsection}{\numberline {1.4}Circuito e Passeio de Euler}{2}
\contentsline {subsection}{\numberline {1.5}Cliques Maximais}{2}
\contentsline {subsection}{\numberline {1.6}Cobertura M\IeC {\'\i }nima por Caminhos em DAG}{2}
\contentsline {subsection}{\numberline {1.7}Componentes Fortemente Conexas}{3}
\contentsline {subsection}{\numberline {1.8}Corte M\IeC {\'\i }nimo Geral (Stoer-Wagner)}{3}
\contentsline {subsection}{\numberline {1.9}Dijkstra}{3}
\contentsline {subsection}{\numberline {1.10}Emparalhamento Bipartido de Custo M\IeC {\'a}ximo}{4}
\contentsline {subsection}{\numberline {1.11}Emparelhamento M\IeC {\'a}ximo e Cobertura M\IeC {\'\i }nima}{4}
\contentsline {subsection}{\numberline {1.12}Emparelhamento M\IeC {\'a}ximo Geral (Edmonds)}{5}
\contentsline {subsection}{\numberline {1.13}Floyd Warshall}{5}
\contentsline {subsection}{\numberline {1.14}Fluxo M\IeC {\'a}ximo de Custo M\IeC {\'\i }nimo (Uso Geral) - Corte M\IeC {\'\i }nimo}{5}
\contentsline {subsection}{\numberline {1.15}Fluxo M\IeC {\'\i }nimo}{6}
\contentsline {subsection}{\numberline {1.16}Pontes, Pontos de Articula\IeC {\c c}\IeC {\~a}o e Componentes Biconexas}{6}
\contentsline {subsection}{\numberline {1.17}Stable Marriage}{7}
\contentsline {subsection}{\numberline {1.18}Topological Sort}{7}
\contentsline {subsection}{\numberline {1.19}Two Satisfiability}{7}
\contentsline {subsection}{\numberline {1.20}Union Find e \IeC {\'A}rvore Geradora M\IeC {\'\i }nima (Kruskal)}{8}
\contentsline {section}{\numberline {2}Geom\IeC {\'e}tricos}{8}
\contentsline {subsection}{\numberline {2.1}Algoritmos B\IeC {\'a}sicos para Circunfer\IeC {\^e}ncia}{8}
\contentsline {subsection}{\numberline {2.2}Algoritmos B\IeC {\'a}sicos para Geom\IeC {\'e}tricos}{9}
\contentsline {subsection}{\numberline {2.3}Algoritmos de Intersec\IeC {\c c}\IeC {\~o}es}{9}
\contentsline {subsection}{\numberline {2.4}C\IeC {\'\i }rculo Gerador M\IeC {\'\i }nimo}{9}
\contentsline {subsection}{\numberline {2.5}Convex Hull (Graham Scan)}{10}
\contentsline {subsection}{\numberline {2.6}Di\IeC {\^a}metro de Pontos e Pol\IeC {\'\i }gono}{10}
\contentsline {subsection}{\numberline {2.7}Dist\IeC {\^a}ncia Esf\IeC {\'e}rica}{10}
\contentsline {subsection}{\numberline {2.8}Estrutura e Base para Geom\IeC {\'e}tricos}{10}
\contentsline {subsection}{\numberline {2.9}Intersec\IeC {\c c}\IeC {\~a}o de Pol\IeC {\'\i }gonos Convexos}{11}
\contentsline {subsection}{\numberline {2.10}Par de Pontos Mais Pr\IeC {\'o}ximos}{11}
\contentsline {subsection}{\numberline {2.11}Verifica\IeC {\c c}\IeC {\~o}es de Ponto em Pol\IeC {\'\i }gono}{11}
\contentsline {section}{\numberline {3}Num\IeC {\'e}ricos}{12}
\contentsline {subsection}{\numberline {3.1}Binomial Modular (e n\IeC {\~a}o modular)}{12}
\contentsline {subsection}{\numberline {3.2}Crivo de Erast\IeC {\'o}tenes}{12}
\contentsline {subsection}{\numberline {3.3}Elimina\IeC {\c c}\IeC {\~a}o de Gauss}{12}
\contentsline {subsection}{\numberline {3.4}Estrutura de Polin\IeC {\^o}mio}{12}
\contentsline {subsection}{\numberline {3.5}Euclides Extendido}{13}
\contentsline {subsection}{\numberline {3.6}Exponencia\IeC {\c c}\IeC {\~a}o modular r\IeC {\'a}pida}{13}
\contentsline {subsection}{\numberline {3.7}Inverso Modular}{13}
\contentsline {subsection}{\numberline {3.8}Log Discreto}{13}
\contentsline {subsection}{\numberline {3.9}Teorema Chin\IeC {\^e}s do Resto}{13}
\contentsline {section}{\numberline {4}Miscel\IeC {\^a}nea}{14}
\contentsline {subsection}{\numberline {4.1}\IeC {\'A}rvore de Intervalos}{14}
\contentsline {subsection}{\numberline {4.2}\IeC {\'A}rvore de Intervalos (c/ Lazy Propagation)}{14}
\contentsline {subsection}{\numberline {4.3}Binary Indexed Tree/Fenwick Tree}{14}
\contentsline {subsection}{\numberline {4.4}Convex Hull Trick}{14}
\contentsline {subsection}{\numberline {4.5}De Brujin Sequence}{15}
\contentsline {subsection}{\numberline {4.6}Decomposi\IeC {\c c}\IeC {\~a}o Heavy Light}{15}
\contentsline {subsection}{\numberline {4.7}Dinic Maximum Flow}{16}
\contentsline {subsection}{\numberline {4.8}Floyd Cycle Finding}{16}
\contentsline {subsection}{\numberline {4.9}Fun\IeC {\c c}\IeC {\~o}es para Datas}{16}
\contentsline {subsection}{\numberline {4.10}Geometria 3D}{16}
\contentsline {subsection}{\numberline {4.11}Knight Distance}{17}
\contentsline {subsection}{\numberline {4.12}Maior Ret\IeC {\^a}ngulo em um Histograma}{18}
\contentsline {subsection}{\numberline {4.13}Polynomial Roots}{18}
\contentsline {subsection}{\numberline {4.14}Range Minimum Query (RMQ)}{18}
\contentsline {subsection}{\numberline {4.15}Romberg - Integral}{18}
\contentsline {subsection}{\numberline {4.16}Rope (via \IeC {\'a}rvore cartesiana)}{19}
\contentsline {section}{\numberline {5}Programa\IeC {\c c}\IeC {\~a}o Din\IeC {\^a}mica}{19}
\contentsline {subsection}{\numberline {5.1}Longest Increasing Subsequence (LIS)}{19}
\contentsline {section}{\numberline {6}Strings}{20}
\contentsline {subsection}{\numberline {6.1}Aho-Corasick}{20}
\contentsline {subsection}{\numberline {6.2}Array de Sufixos n*lg(n)}{20}
\contentsline {subsection}{\numberline {6.3}Busca de Strings (KMP)}{21}
\contentsline {subsection}{\numberline {6.4}Hash de Strings}{21}
\contentsline {section}{\numberline {7}Dadalto}{21}
\contentsline {subsection}{\numberline {7.1}<Nome do algoritmo>}{21}
\contentsline {section}{\numberline {8}Drazy}{21}
\contentsline {subsection}{\numberline {8.1}Articulation Points and Bridges}{21}
\contentsline {subsection}{\numberline {8.2}BIT with O(log n) Binary Search}{21}
\contentsline {subsection}{\numberline {8.3}Centroid Decomposition}{22}
\contentsline {subsection}{\numberline {8.4}Divid and Conquer DP}{22}
\contentsline {subsection}{\numberline {8.5}Dual String Hash (with is_eq and is_less)}{22}
\contentsline {subsection}{\numberline {8.6}Dynamic Convex Hull trick}{22}
\contentsline {subsection}{\numberline {8.7}FFT}{23}
\contentsline {subsection}{\numberline {8.8}Geometrics}{23}
\contentsline {subsection}{\numberline {8.9}Integer Ternary Search}{24}
\contentsline {subsection}{\numberline {8.10}KMP}{24}
\contentsline {subsection}{\numberline {8.11}LCA}{24}
\contentsline {subsection}{\numberline {8.12}Maxflow}{24}
\contentsline {subsection}{\numberline {8.13}Mincost Maxflow}{25}
\contentsline {subsection}{\numberline {8.14}MOS}{25}
\contentsline {subsection}{\numberline {8.15}Path queries on trees}{25}
\contentsline {subsection}{\numberline {8.16}Regular Convex Hull Trick}{26}
\contentsline {subsection}{\numberline {8.17}Segment Tree}{26}
\contentsline {subsection}{\numberline {8.18}Segment Tree with Lazy Propagation}{26}
\contentsline {subsection}{\numberline {8.19}Sieve / Phi / Integer Factorization}{27}
\contentsline {subsection}{\numberline {8.20}Sparse table}{27}
\contentsline {subsection}{\numberline {8.21}Sqrt decomposition}{27}
\contentsline {subsection}{\numberline {8.22}Suffix Array}{28}
\contentsline {subsection}{\numberline {8.23}Suffix Automaton}{28}
\contentsline {subsection}{\numberline {8.24}Suffix Automaton}{28}
\contentsline {section}{\numberline {9}Ataide}{28}
\contentsline {subsection}{\numberline {9.1}<Nome do algoritmo>}{28}
\contentsline {section}{\numberline {10}Matem\IeC {\'a}tica}{28}
\contentsline {subsection}{\numberline {10.1}Geometria}{28}
\contentsline {subsection}{\numberline {10.2}Rela\IeC {\c c}\IeC {\~o}es Binomiais}{29}
\contentsline {subsection}{\numberline {10.3}Equa\IeC {\c c}\IeC {\~o}es Diofantinas}{29}
\contentsline {subsection}{\numberline {10.4}Fibonacci}{29}
\contentsline {subsection}{\numberline {10.5}Problemas cl\IeC {\'a}ssicos}{29}
\contentsline {subsection}{\numberline {10.6}S\IeC {\'e}ries Num\IeC {\'e}ricas}{29}
\contentsline {subsection}{\numberline {10.7}Matrizes e Determinantes}{30}
\contentsline {subsection}{\numberline {10.8}Probabilidades}{30}
\contentsline {subsection}{\numberline {10.9}Teoria dos N\IeC {\'u}meros}{30}
\contentsline {subsection}{\numberline {10.10}Prime counting function ($\pi (x)$)}{30}
\contentsline {subsection}{\numberline {10.11}Partition function}{30}
\contentsline {subsection}{\numberline {10.12}Catalan numbers}{30}
\contentsline {subsection}{\numberline {10.13}Stirling numbers of the first kind}{30}
\contentsline {subsection}{\numberline {10.14}Stirling numbers of the second kind}{30}
\contentsline {subsection}{\numberline {10.15}Bell numbers}{31}
\contentsline {subsection}{\numberline {10.16}Tur\IeC {\'a}n's theorem}{31}
\contentsline {subsection}{\numberline {10.17}Generating functions}{31}
\contentsline {subsection}{\numberline {10.18}Polyominoes}{31}
\contentsline {subsection}{\numberline {10.19}The twelvefold way (from Stanley)}{31}
\contentsline {subsection}{\numberline {10.20}Common integral substitutions}{31}
\contentsline {subsection}{\numberline {10.21}Table of non-trigonometric integrals}{31}
\contentsline {subsection}{\numberline {10.22}Table of trigonometric integrals}{31}
\contentsline {subsection}{\numberline {10.23}Centroid of a polygon}{32}
